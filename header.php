<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>
</head>
<body id="modalParrent" <?php body_class(); ?>>
	<header class="header-no-image clear" role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top nav-no-image">
		<div class="container">
				<div class="navbar-header">
					<button id="nav-menu-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					    aria-expanded="false">
						<div id="navbar-hamburger">
							<span class="sr-only">Toggle navigation</span> Menu
							<i class="fa fa-bars"></i>
						</div>
						<div id="navbar-close" class="hidden">
							<span class="glyphicon glyphicon-remove"></span>
						</div>
					</button>
					<?php krs_headlogo(); ?>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->
		<div class="headline-no-image">
			<h1 class="title-no-image text-center"><?php the_title(); ?></h1>
		</div>
	</header>
	<!-- /header -->
