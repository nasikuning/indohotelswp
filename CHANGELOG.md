## 0.2.6 (30 July, 2018)
* add webfonts

## 0.2.5 (24 July, 2018)
* disable posts_custom_column_views

## 0.2.4 (24 July, 2018)
* Edit code in function maybe_cleanup() OptionTree, edit karisma_text_domain > 'karisma_text_domain' in where clause

## 0.2.3 (10 July, 2018)
* Tidy up styling code
* Add minify files

## 0.2.2 (27 March, 2018)
* Migration endpoint from jogjahotels to indohotels

## 0.2.1 (22 March, 2018)
* Add feature for change header layout
* Add main image in gallery

## 0.1.8 (2 March, 2018)
* Fix Css custom options

## 0.1.7 (26 April, 2017)
* Make Parrent menu navbar clickable
