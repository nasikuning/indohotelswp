<div class="clearfix"></div>
<!-- The content of your page would go here. -->

	<footer class="footer-distributed">
		<div class="container">
			<div class="footer-info">
				<div class="row">
					<div class="col-md-3">
						<?php if ( ! dynamic_sidebar( 'footer1' ) ) : ?>
						<?php endif; ?>
					</div>
					<div class="col-md-6">
						<?php if ( ! dynamic_sidebar( 'footer2' ) ) : ?>
						<?php endif; ?>
					</div>
					<div class="col-md-3">
						<?php if ( ! dynamic_sidebar( 'footer3' ) ) : ?>
						<?php endif; ?>
					</div>
				</div>
			</div><!-- end .footer-info -->

			<div class="footer-icons">
				<?php krs_sn(); ?>
			</div>

			<nav class="nav text-center nav-bottom">
				<?php karisma_nav_footer(); ?>
			</nav> <!-- End nav -->

		</div><!-- end .container -->
		<div class="footer-credits">
			<?php echo ot_get_option('krs_footcredits'); ?>
		</div>
	</footer>
</div>
<!-- /wrapper -->

<?php wp_footer(); ?>

</body>

</html>
