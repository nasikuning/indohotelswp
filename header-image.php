<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header class="header-image-page clear" <?php krs_header_cover(); ?> role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<div class="navbar-brand">
							<?php krs_headlogo(); ?>
					</div>
					<button id="nav-menu-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<div id="navbar-hamburger">
							<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
						</div>
						<div id="navbar-close" class="hidden">
							<span class="glyphicon glyphicon-remove"></span>
						</div>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->
		<div class="headline">
			<h1 class="title text-center"><?php the_title(); ?></h1>
		</div>
		<div class="overlay"></div>
	</header>
	<!-- /header -->