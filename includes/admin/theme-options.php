<?php
/**
 * Initialize the custom theme options.
 */
add_action('admin_init', '_custom_theme_options', 1);

/**
 * Build the custom settings & update OptionTree.
 */
function _custom_theme_options()
{

  /* OptionTree is not loaded yet, or this is not an admin request
  * if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
   * return false;
    */
    /**
     * Get a copy of the saved settings array.
     */
    $saved_settings = get_option('option_tree_settings', array());

    /**
     * Custom settings array that will eventually be
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
    'contextual_help' => array(
      'content'       => array(
        array(
          'id'        => 'option_types_help',
          'title'     => __('Option Types', karisma_text_domain),
          'content'   => __('<p>Help content goes here!</p>', karisma_text_domain)
          )
        ),
      'sidebar'       => __('<p>Sidebar content goes here!</p>', karisma_text_domain)
      ),
    'sections'        => array(
      array(
        'id'          => 'general_tab',
        'title'       => __('General', karisma_text_domain)
        ),
      array(
        'id'          => 'homepage_tab',
        'title'       => __('Homepage', karisma_text_domain)
        ),
      array(
        'id'          => 'single_tab',
        'title'       => __('Single', karisma_text_domain)
        ),
      array(
        'id'          => 'header_tab',
        'title'       => __('Header', karisma_text_domain)
        ),
      array(
        'id'          => 'footer_tab',
        'title'       => __('Footer', karisma_text_domain)
        ),
      array(
        'id'          => 'stylish_tab',
        'title'       => __('Stylish', karisma_text_domain)
        ),
      array(
        'id'          => 'layout_tab',
        'title'       => __('Layout', karisma_text_domain)
        ),
      array(
        'id'          => 'social_tab',
        'title'       => __('Social Network', karisma_text_domain)
        ),
      array(
        'id'          => 'map_tab',
        'title'       => __('Maps', karisma_text_domain)
        ),
      array(
        'id'          => 'cpt_tab',
        'title'       => __('CPT Option', karisma_text_domain)
        ),
      array(
        'id'          => 'comment_tab',
        'title'       => __('Comments', karisma_text_domain)
        ),
      array(
        'id'          => 'license_tab',
        'title'       => __('Theme License', karisma_text_domain)
        ),
      array(
        'id'          => 'themeinfo_tab',
        'title'       => __('System Information', karisma_text_domain)
        ),
      ),
    'settings'        => array(
      array(
        'id'          => 'krs_logo_actived',
        'label'       => __('Enable Primary logo image?', karisma_text_domain),
        'desc'        => __('Please On for enable logo image and Off for disable logo image.', karisma_text_domain),
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
      array(
        'id'          => 'krs_logo',
        'label'       => __('Your logo image', karisma_text_domain),
        'desc'        => __('Upload your logo image and type full path here. Default width:232px and height:90px.', karisma_text_domain),
        'std'         => krs_url .'asset/images/logo.svg',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'krs_logo_actived:is(on),krs_logo_actived:not()',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_logo2_actived',
        'label'       => __('Enable Secondary logo image?', karisma_text_domain),
        'desc'        => __('This is optional if you have more than 1 logo images.', karisma_text_domain),
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
      array(
        'id'          => 'krs_logo2',
        'label'       => __('Your logo image', karisma_text_domain),
        'desc'        => __('Upload your logo image and type full path here. Default width:232px and height:90px.', karisma_text_domain),
        'std'         => krs_url .'asset/images/logo.svg',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'krs_logo2_actived:is(on),krs_logo2_actived:not()',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_favicon',
        'label'       => __('Your Favicon', karisma_text_domain),
        'desc'        => __('Upload your ico image or type full path here.', karisma_text_domain),
        'std'         => krs_url .'asset/images/favicon.ico',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_appleicon',
        'label'       => __('Your Apple Touch Icon', karisma_text_domain),
        'desc'        => __('Upload a 16 x 16 px image that will represent your website\'s favicon. You can refer to this link for more information on how to make it: http://www.favicon.cc/', karisma_text_domain),
        'std'         => krs_url .'asset/images/touch.png',
        'type'        => 'upload',
        'section'     => 'general_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_head',
        'label'       => __('Head Code', karisma_text_domain),
        'desc'        => __('Enter the code which you need to place before closing tag. (ex: Google Webmaster Tools verification, Bing Webmaster Center, BuySellAds Script, Alexa verification etc.)', karisma_text_domain),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_footer',
        'label'       => __('Footer Script', karisma_text_domain),
        'desc'        => __('If you need to add scripts to your footer, do so here. (ex: Google Analytics).', karisma_text_domain),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '10',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_footcredits',
        'label'       => __('Text footer credits', karisma_text_domain),
        'desc'        => __('You can change copyright footer and use your own custom text.', karisma_text_domain),
        'std'         => '© 2018 Powered by <a href="//indohotels.id">PT. Indohotels Booking Systems</a>',
        'type'        => 'textarea-simple',
        'section'     => 'general_tab',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      /* Homepage */
      array(
        'id'          => 'krs_gallery',
        'label'       => __('Home Slider', karisma_text_domain),
        'desc'        => __('You can upload your photos for home slideshow.', karisma_text_domain),
        'std'         => '',
        'type'        => 'gallery',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_ros_in_title',
        'label'       => __('Welcome Title', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_tab',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_ros_in',
        'label'       => __('Welcome Text', karisma_text_domain),
        'desc'        => __('Type your description Hotel.', karisma_text_domain),
        'std'         => 'Your main description',
        'type'        => 'textarea',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_background_text1',
        'label'       => __('Welcome Background', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
        // Section Room
      array(
        'id'          => 'krs_room_actived',
        'label'       => __('Enable Section 2 listing on homepage?', karisma_text_domain),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'homepage_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
        array(
          'id'          => 'krs_headline_section2',
          'label'       => __('Title Headline Section 2', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'text',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_room_actived:is(on),krs_room_actived:not()',
          'operator'    => 'and'
          ),
        array(
          'id'          => 'krs_section_2',
          'label'       => __('List of page', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'select',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_room_actived:is(on),krs_room_actived:not()',
          'operator'    => 'and',
          'choices'     => array(
            array(
              'value'       => 'deals',
              'label'       => __('Deals', karisma_text_domain),
              'src'         => ''
              ),
            array(
              'value'       => 'rooms',
              'label'       => __('Rooms', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'hotel-info',
              'label'       => __('Hotel Information', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'beauty-retail',
              'label'       => __('Other Facilities', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'restaurant-cafe',
              'label'       => __('Restaurant & Cafe', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'meetings-events',
              'label'       => __('Meeting & Events', karisma_text_domain),
              'src'         => ''
              )
            )
          ),
          // End of Section
          // Section Room
      array(
        'id'          => 'krs_section3_actived',
        'label'       => __('Enable Section 3 listing on homepage?', karisma_text_domain),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'homepage_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
        array(
          'id'          => 'krs_headline_section3',
          'label'       => __('Title Headline Section 3', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'text',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_section3_actived:is(on),krs_section3_actived:not()',
          'operator'    => 'and'
          ),
        array(
          'id'          => 'krs_section_3',
          'label'       => __('List of page', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'select',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_section3_actived:is(on),krs_section3_actived:not()',
          'operator'    => 'and',
          'choices'     => array(
            array(
              'value'       => 'deals',
              'label'       => __('Deals', karisma_text_domain),
              'src'         => ''
              ),
            array(
              'value'       => 'rooms',
              'label'       => __('Rooms', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'hotel-info',
              'label'       => __('Hotel Information', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'beauty-retail',
              'label'       => __('Other Facilities', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'restaurant-cafe',
              'label'       => __('Restaurant & Cafe', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'meetings-events',
              'label'       => __('Meeting & Events', karisma_text_domain),
              'src'         => ''
              )
            )
          ),
      array(
        'id'          => 'krs_section3_bg',
        'label'       => __('Section 3 background', karisma_text_domain),
        'desc'        => __('You can change section 3 background color from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'operator'    => 'and',
        'condition'   => 'krs_section3_actived:is(on),krs_section3_actived:not()',
        ),
          // End of Section
          // Section Room
      array(
        'id'          => 'krs_section4_actived',
        'label'       => __('Enable Section 4 listing on homepage?', karisma_text_domain),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'homepage_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
        array(
          'id'          => 'krs_headline_section4',
          'label'       => __('Title Headline Section 4', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'text',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_section4_actived:is(on),krs_section4_actived:not()',
          'operator'    => 'and'
          ),
        array(
          'id'          => 'krs_section_4',
          'label'       => __('List of page', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'select',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_section4_actived:is(on),krs_section4_actived:not()',
          'operator'    => 'and',
          'choices'     => array(
            array(
              'value'       => 'deals',
              'label'       => __('Deals', karisma_text_domain),
              'src'         => ''
              ),
            array(
              'value'       => 'rooms',
              'label'       => __('Rooms', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'hotel-info',
              'label'       => __('Hotel Information', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'beauty-retail',
              'label'       => __('Other Facilities', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'restaurant-cafe',
              'label'       => __('Restaurant & Cafe', karisma_text_domain),
              'src'         => ''
            ),
            array(
              'value'       => 'meetings-events',
              'label'       => __('Meeting & Events', karisma_text_domain),
              'src'         => ''
              )
            )
          ),
        array(
          'id'          => 'krs_cat_section4',
          'label'       => __('Categories Section 4', karisma_text_domain),
          'desc'        => '',
          'std'         => '',
          'type'        => 'select',
          'section'     => 'homepage_tab',
          'rows'        => '3',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'krs_section4_actived:is(on),krs_section4_actived:not()',
          'operator'    => 'and',
          'choices'    => my_cat(),
          ),
      array(
        'id'          => 'krs_section4_bg',
        'label'       => __('Section 4 background', karisma_text_domain),
        'desc'        => __('You can change section 4 background color from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'homepage_tab',
        'rows'        => '3',
        'operator'    => 'and',
        'condition'   => 'krs_section4_actived:is(on),krs_section4_actived:not()',
        ),
          // End of Section

      array(
        'id'          => 'gallery-slideshow',
        'label'       => __('Enable slideshow at middle?', karisma_text_domain),
        'desc'        => __('If you want enable this please go to <a href="http://hotel.indohotels.id/website-gold/wp-admin/edit.php?post_type=gallery" target="_blank">Add Gallery Post</a> with "home" post type.', karisma_text_domain),
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'homepage_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
  array(
    'id'          => 'krs_active_related',
    'label'       => __('Enable related post?', karisma_text_domain),
    'desc'        => __('Please select on for enable related post in single and select off for disable related post in single.', karisma_text_domain),
    'std'         => 'on',
    'type'        => 'on-off',
    'section'     => 'single_tab',
    'rows'        => '',
    'post_type'   => '',
    'taxonomy'    => '',
    'min_max_step'=> '',
    'class'       => '',
    'condition'   => '',
    'choices'     => array(
      array(
        'value'       => 'on',
        'label'       => __('on', karisma_text_domain),
        'src'         => ''
        ),
      array(
        'value'       => 'off',
        'label'       => __('off', karisma_text_domain),
        'src'         => ''
        )
      )
    ),
  array(
    'id'          => 'krs_taxonomy_relpost',
    'label'       => __('Related by taxonomy', karisma_text_domain),
    'desc'        => __('Please select what do you want related by taxonomy.', karisma_text_domain),
    'std'         => '',
    'type'        => 'select',
    'section'     => 'single_tab',
    'rows'        => '',
    'post_type'   => '',
    'taxonomy'    => '',
    'min_max_step'=> '',
    'class'       => '',
    'condition'   => 'krs_active_related:is(on),krs_active_related:off()',
    'choices'     => array(
      array(
        'value'       => 'category',
        'label'       => __('Category', karisma_text_domain),
        ),
      array(
        'value'       => 'tags',
        'label'       => __('Tags', karisma_text_domain),
        ),
      array(
        'value'       => 'title',
        'label'       => __('Title', karisma_text_domain),
        )
      )
    ),
  array(
    'id'          => 'krs_taxonomy_count',
    'label'       => __('Number of related by taxonomy', karisma_text_domain),
    'desc'        => __('Please select what do you want number of related by taxonomy.', karisma_text_domain),
    'std'         => '6',
    'type'        => 'text',
    'section'     => 'single_tab',
    'rows'        => '',
    'post_type'   => '',
    'taxonomy'    => '',
    'min_max_step'=> '',
    'class'       => '',
    'condition'   => 'krs_active_related:is(on),krs_active_related:off()',

    ),
      /* Header */
      array(
        'id'          => 'krs_head_hotelinfo_actived',
        'label'       => __('Enable Hotel info on header?', karisma_text_domain),
        'desc'        => __('Hotel info such as address, phone and email.', karisma_text_domain),
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'header_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
      /* Footer */
      array(
        'label' => __('Footer Left', karisma_text_domain),
        'id' => 'footer_left',
        'type' => 'tab',
        'section'     => 'footer_tab',
        ),
      array(
        'id'          => 'krs_footer_logo_actived',
        'label'       => __('Enable Footer logo image?', karisma_text_domain),
        'desc'        => __('Please check on for enable logo image and check off for disable logo image.', karisma_text_domain),
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'on',
            'label'       => __('On', karisma_text_domain),
            'src'         => ''
            ),
          array(
            'value'       => 'off',
            'label'       => __('Off', karisma_text_domain),
            'src'         => ''
            )
          )
        ),
      array(
        'id'          => 'krs_footer_logo',
        'label'       => __('Your logo image', karisma_text_domain),
        'desc'        => __('Upload your logo image and type full path here. Default width:232px and height:90px.', karisma_text_domain),
        'std'         => krs_url .'asset/images/logo.svg',
        'type'        => 'upload',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'krs_footer_logo_actived:is(on),krs_footer_logo_actived:not()',
        'operator'    => 'and'
        ),
      array(
        'label' => __('Footer Center', karisma_text_domain),
        'id' => 'footer_center',
        'type' => 'tab',
        'section'     => 'footer_tab',
        ),
      array(
        'id'          => 'krs_address',
        'label'       => __('Address', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_phone',
        'label'       => __('Phone', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_fax',
        'label'       => __('Fax', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_whatsapp',
        'label'       => __('Whatsapp', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_bbm',
        'label'       => __('BBM', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_email',
        'label'       => __('Email', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),

      array(
        'label' => __('Footer Right', karisma_text_domain),
        'id' => 'footer_right',
        'type' => 'tab',
        'section'     => 'footer_tab',
        ),
      array(
        'id'          => 'krs_about_us_title',
        'label'       => __('About Us', karisma_text_domain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_about_us',
        'label'       => '',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'footer_tab',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
        ),

      /*  Stylish Website */
      array(
        'label' => __('Main Style', karisma_text_domain),
        'id' => 'main_style',
        'type' => 'tab',
        'section'     => 'stylish_tab',
        ),
      array( // Google Font API
            'id'          => 'google-font-api',
            'label'       => __('Google Font API Key', karisma_text_domain),
            'desc'        => __('Enter your Google Font API Key to ensure updates of the google font library.', karisma_text_domain),
            'std'         => '',
            'type'        => 'text',
            'section'     => 'stylish_tab',
            'class'       => ''
      ),
      array(
        'id'          => 'krs_main_background',
        'label'       => __('Main background', karisma_text_domain),
        'desc'        => __('You can change main background color from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_main_colorpicker',
        'class'       => 'ot-colorpicker-opacity',
        'label'       => __('Change main color', karisma_text_domain),
        'desc'        => __('You can change main color website.', karisma_text_domain),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'stylish_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_secound_colorpicker',
        'class'       => 'ot-colorpicker-opacity',
        'label'       => __('Change secondary color', karisma_text_domain),
        'desc'        => __('You can change secondary color website.', karisma_text_domain),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'stylish_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_tertiary_colorpicker',
        'class'       => 'ot-colorpicker-opacity',
        'label'       => __('Change tertiary color', karisma_text_domain),
        'desc'        => __('You can change tertiary color website.', karisma_text_domain),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'stylish_tab',
        'rows'        => '2',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_main_fonts',
        'label'       => __('Change Body typography', karisma_text_domain),
        'desc'        => __('You can change main typography themes.', karisma_text_domain),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'allowed_fields' => array('font-family','font-size','font-color'),
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_font_hover',
        'label'       => __('Change Hover color text', karisma_text_domain),
        'desc'        =>'',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'stylish_tab',
        'rows'        => '1',
        'condition'   => '',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_lead_fonts',
        'label'       => __('Change Lead typography', karisma_text_domain),
        'desc'        => __('You can change lead typography such as H1, H2, H3 text.', karisma_text_domain),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'allowed_fields' => array('font-family','font-size','font-color'),
        'operator'    => 'and'
        ),
      array(
        'label' => __('Head Style', karisma_text_domain),
        'id' => 'head_style',
        'type' => 'tab',
        'section'     => 'stylish_tab',
        ),
      array(
        'id'          => 'krs_nav_style',
        'label'       => __('Nav Color', karisma_text_domain),
        'desc'        => __('You can change navigation color from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_nav_background',
        'label'       => __('Nav background', karisma_text_domain),
        'desc'        => __('You can change navigation background color from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'label' => __('Footer Style', karisma_text_domain),
        'id' => 'footer_style',
        'type' => 'tab',
        'section'     => 'stylish_tab',
        ),
      array(
        'id'          => 'krs_footer_style',
        'label'       => __('Footer Style', karisma_text_domain),
        'desc'        => __('You can change footer style from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
      array(
        'id'          => 'krs_footer_background',
        'label'       => __('Footer Background', karisma_text_domain),
        'desc'        => __('You can change footer background from here.', karisma_text_domain),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'stylish_tab',
        'rows'        => '3',
        'operator'    => 'and'
        ),
    /*
    * Layout
    */
      array(
        'id'          => 'layout-header',
        'label'       => 'Layout Header',
        'desc'        => 'Select your favorite layout for header',
        'std'         => 'header-1',
        'type'        => 'radio-image',
        'section'     => 'layout_tab',
        'choices'     => array(
          array(
            'value'       => 'header-1',
            'label'       => 'Header 1',
            'src'         => 'https://hotel.indohotels.id/assets/vendor/images/layout/header-1.png'
          ),
          array(
            'value'       => 'header-2',
            'label'       => 'Header 2',
            'src'         => 'https://hotel.indohotels.id/assets/vendor/images/layout/header-2.png'
          )
        )
      ),

    /*
    * Layout Footer
    */
      array(
        'id'          => 'layout-footer',
        'label'       => 'Layout Footer',
        'desc'        => 'Select your favourite layout for footer',
        'std'         => 'footer-alpha',
        'type'        => 'radio-image',
        'section'     => 'layout_tab',
        'choices'     => array(
          array(
            'value'       => 'footer-alpha',
            'label'       => 'Footer Alpha',
            'src'         => 'https://hotel.indohotels.id/assets/vendor/images/layout/footer-1.png'
          ),
          array(
            'value'       => 'footer-beta',
            'label'       => 'Footer Beta',
            'src'         => 'https://hotel.indohotels.id/assets/vendor/images/layout/footer-2.png'
          )
        )
      ),

/*
  * Karisma Social network
  */
array(
  'label'       => __('Enable social link on Footer', karisma_text_domain),
  'id'          => 'krs_head_social_activated',
  'type'        => 'on-off',
  'desc'        => __('Please On for enable footer social and Off for disable footer social.', karisma_text_domain),
  'choices'     => array(
    array(
      'label'       => 'On',
      'value'       => 'on'
      ),
    array(
      'label'       => 'Off',
      'value'       => 'off'
      )
    ),
  'std'         => 'on',
  'section'     => 'social_tab'
  ),

array(
  'label'       => __('Enable social shared post?', karisma_text_domain),
  'id'          => 'krs_active_shared',
  'type'        => 'on-off',
  'desc'        => __('Please On for enable social shared post and Off for disable shared post.', karisma_text_domain),
  'choices'     => array(
    array(
      'label'       => 'On',
      'value'       => 'on'
      ),
    array(
      'label'       => 'Off',
      'value'       => 'off'
      )
    ),
  'std'         => 'on',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Facebook application id', karisma_text_domain),
  'id'          => 'krs_facebook_app_id',
  'type'        => 'text',
  'desc'       => __('Fill with your facebook application id. To add a Facebook Comments Box you will need to create a Facebook App first. Go to https://developers.facebook.com/apps/ and create a new app. Enter a name and define the locale for the app.', karisma_text_domain),
  'std'         => '231372170246816',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Facebook URL', karisma_text_domain),
  'id'          => 'krs_fb_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your facebook url here, for example https://facebook.com/example (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Twitter URL', karisma_text_domain),
  'id'          => 'krs_tweet_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your twitter url here, for example https://twitter.com/example (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Instagram URL', karisma_text_domain),
  'id'          => 'krs_instagram_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your Instagram url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Youtube URL', karisma_text_domain),
  'id'          => 'krs_youtube_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your youtube url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Gplus URL', karisma_text_domain),
  'id'          => 'krs_gplus_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your google plus url here. For example http://plus.google.com/3894293874219340  (use http://)', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Linkedin URL', karisma_text_domain),
  'id'          => 'krs_in_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your linkedin url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Dribble URL', karisma_text_domain),
  'id'          => 'krs_dribble_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your dribble url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Flickr URL', karisma_text_domain),
  'id'          => 'krs_flickr_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your flickr url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('Tumblr URL', karisma_text_domain),
  'id'          => 'krs_tumblr_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your tumblr url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
  'label'       => __('TripAdvisor', karisma_text_domain),
  'id'          => 'krs_tripadvisor_sn',
  'type'        => 'text',
  'desc'       => __('Please enter your tripadvisor url here (use http://).', karisma_text_domain),
  'std'         => '',
  'section'     => 'social_tab'
  ),
array(
    'label'       => __('Line', karisma_text_domain),
    'id'          => 'krs_line_sn',
    'type'        => 'text',
    'desc'       => __('Please enter your line url here (use http://).', karisma_text_domain),
    'std'         => '',
    'section'     => 'social_tab'
    ),
array(
  'label'       => __('RSS', karisma_text_domain),
  'id'          => 'krs_rss_sn',
  'type'        => 'on-off',
  'desc'        => __('Please on for enable RSS link and off for disable RSS link.', karisma_text_domain),
  'choices'     => array(
    array(
      'label'       => 'On',
      'value'       => 'on'
      ),
    array(
      'label'       => 'Off',
      'value'       => 'off'
      )
    ),
  'std'         => 'on',
  'section'     => 'social_tab'
  ),

  /*
  * Maps
  */
  array(
    'label'       => __('Map', karisma_text_domain),
    'id'          => 'krs_map',
    'type'        => 'text',
    'desc'        => __('Please add your google map link', karisma_text_domain),
    'std'         => '',
    'section'     => 'map_tab'
    ),
  /*
  * Custom Post Type Options
  */
  array(
    'label'       => __('Enable Custom Post Type?', karisma_text_domain),
    'id'          => 'krs_cpt_activated',
    'type'        => 'on-off',
    'desc'        => __('Please On for enable Custom Post Type and Off for disable Custom Post Type.', karisma_text_domain),
    'choices'     => array(
      array(
        'label'       => 'On',
        'value'       => 'on'
        ),
      array(
        'label'       => 'Off',
        'value'       => 'off'
        )
      ),
    'std'         => 'on',
    'section'     => 'cpt_tab'
    ),
array(
    'label'       => __('CPT Name', karisma_text_domain),
    'id'          => 'krs_cpt_name',
    'type'        => 'list-item',
    'desc'       => __('', karisma_text_domain),
    'std'         => 'Addons',
    'section'     => 'cpt_tab'
    ),
  /*
  * Karisma comment
  */
  array(
    'label'       => __('Enable Facebook comment?', karisma_text_domain),
    'id'          => 'krs_facebook_com_activated',
    'type'        => 'on-off',
    'desc'        => __('Please On for enable Facebook comment and Off for disable Facebook comment.', karisma_text_domain),
    'choices'     => array(
      array(
        'label'       => 'On',
        'value'       => 'on'
        ),
      array(
        'label'       => 'Off',
        'value'       => 'off'
        )
      ),
    'std'         => 'on',
    'section'     => 'comment_tab'
    ),
  array(
    'label'       => __('Enable Default comment?', karisma_text_domain),
    'id'          => 'krs_def_com_activated',
    'type'        => 'on-off',
    'desc'        => __('Please On for enable Default comment and Off for disable Default comment.', karisma_text_domain),
    'choices'     => array(
      array(
        'label'       => 'On',
        'value'       => 'on'
        ),
      array(
        'label'       => 'Off',
        'value'       => 'off'
        )
      ),
    'std'         => 'on',
    'section'     => 'comment_tab'
    ),
  /* theme info */
  array(
    'label'       => __('System information', karisma_text_domain),
    'id'          => 'krs_textblock',
    'type'        => 'textblock',
    'desc'        => krs_themes_info(),
    'section'     => 'themeinfo_tab'
    ),
  array(
    'label'       => __('Theme License', karisma_text_domain),
    'id'          => 'krs_textlicense',
    'type'        => 'textblock',
    'desc'        => krs_license(),
    'section'     => 'license_tab'
    ),
  )
);

    /* allow settings to be filtered before saving */
    $custom_settings = apply_filters('option_tree_settings_args', $custom_settings);

    /* settings are not the same update the DB */
    if ($saved_settings !== $custom_settings) {
        update_option('option_tree_settings', $custom_settings);
    }
}
