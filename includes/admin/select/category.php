<?php function my_cat() {
    $categories = get_categories();
    $cat_array = array();
    foreach ($categories as $category) {
        $cat_array  = array(
            'value'       => $category->slug,
            'label'       => __( $category->name, karisma_text_domain ),
            'src'         => ''
        );
    }
    return($cat_array);

}