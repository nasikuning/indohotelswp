  jQuery(document).ready(function (jQuery) {

    // hidden arrow in slider, if only one image
    var numItems = jQuery('.item').length;
    if (numItems < 2 ) {
      jQuery('.item a').css('display', 'none');
    }

    jQuery('#homeSliderCarousel').carousel({
      interval: 4000
    });

    jQuery('#carousel-text').html(jQuery('#slide-content-0').html());

    //Handles the carousel thumbnails
    jQuery('[id^=carousel-selector-]').click(function () {
      var id_selector = jQuery(this).attr("id");
      var id = id_selector.substr(id_selector.length - 1);
      var id = parseInt(id);
      jQuery('#homeSliderCarousel').carousel(id);
    });

    // When the carousel slides, auto update the text
    jQuery('#homeSliderCarousel').on('slid', function (e) {
      var id = jQuery('.item.active').data('slide-number');
      jQuery('#carousel-text').html(jQuery('#slide-content-' + id).html());
    });

  });

  jQuery(document).ready(function () {
    jQuery('#image-popups').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function (item) {
          return item.el.attr('title') + '<small></small>';
        }
      }
    });
    jQuery('.thumb-deals').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function (item) {
          return item.el.attr('title') + '<small></small>';
        }
      }
    });
  });

  jQuery(document).ready(function () {
    jQuery('.home-gallery').owlCarousel({
      loop: false,
      margin: 18,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      loop: true,
      nav: true,
      navText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'>"],
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          margin: 5,
        },
        600: {
          items: 3,
          margin: 8,
        },
        1024: {
          items: 4,
          margin: 14,
        }
      }
    });

    /* Testimonial */
    jQuery('.home-text-slide').owlCarousel({
      loop: false,
      margin: 18,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      loop: true,
      dots: true,
      nav: true,
      navText: ["<span class='glyphicon glyphicon-chevron-left'></span>",
      "<span class='glyphicon glyphicon-chevron-right'>"],
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          margin: 5,
        },
        600: {
          items: 1,
          margin: 8,
        },
        1024: {
          items: 1,
          margin: 14,
        }
      }
    });
  });

  jQuery(document).ready(function () {
    jQuery('.gallery-view').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.gallery-nav'
    });
    jQuery('.gallery-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.gallery-view',
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]

    });
    jQuery('.slider-nav').show();

  });

  /* Smooth scroll */
  jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 70) {
      jQuery('#mainNav').addClass('nav-background');
    } else {
      jQuery('#mainNav').removeClass('nav-background');
    }
  });
  /* Smooth scroll */
  jQuery(document).ready(function (jQuery) {
    jQuery("#btn-book").click(function () {
      jQuery('body').animate({
        scrollTop: jQuery("#booknow").offset().top
      }, 2000);
    });
    jQuery('#nav-menu-mobile').click(function () {
      jQuery('#navbar-hamburger').toggleClass('hidden');
      jQuery('#navbar-close').toggleClass('hidden');
    });
  });

  jQuery(function ($) {
    // Bootstrap menu magic
      if ($(window).width() < 768) {
        $(".dropdown-toggle").attr('data-toggle', 'dropdown');
      } else {
        $(".dropdown-toggle").removeAttr('data-toggle', 'dropdown');
      }
  });

// jQuery(document).ready(function() {
//     jQuery('li').has('ul.sub-menu').addClass('dropdown');
//     jQuery('.dropdown a.nav-li').append('<b class="caret"></b>');
//     jQuery('.dropdown-menu>li>a .caret, ul.monav>li>a .caret').remove();
//     jQuery('.mobmenu li.has-submenu ul').removeClass('dropdown-menu');
// });

/* show/hide booking form */
jQuery(document).ready(function(){
  jQuery('.book_trigger').click(function(){
      jQuery('.book-trigger').toggle();
  });
});

/* stick when scroll booking form */
jQuery(document).ready(function(){
  var header = jQuery(".canstick");
  jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= window.innerHeight) {
      header.addClass("fixed");
    } else {
      header.removeClass("fixed");
    }
  });
});

/* make default gallery wordpress have popup */
jQuery(document).ready(function () {
  jQuery('[id*=gallery-]').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      attr: 'alt',
      titleSrc: function (item) {
        return item.el.find('img').attr('alt') + '<small></small>';
      }
    }
  });
});

/* arrange submenu depth */
jQuery(document).ready(function () {
  jQuery('.menu-item-has-children a.dropdown-toggle').on("click", function(e){
    jQuery(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});

jQuery(document).ready(function() {
  jQuery('.popup-gmaps').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false,

    iframe: {
      markup: '<div class="mfp-iframe-scaler">'+
             '<div class="mfp-close"></div>'+
             '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
             '</div>',
      patterns: {
         gmaps: {
             index: '//maps.google.',
             src: '%id%&output=embed'
         }
      },
      srcAction: 'iframe_src',
     }
	});
});
