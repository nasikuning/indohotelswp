// Animated css
jQuery(document).ready(function() {
	jQuery('.box-bg, .text-header, #intro-box, .bookid-wrapper, #ros-in').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('#ros-in h2, .hotel-info-home h2, .room-section h2, .thumbnails, .title-gallery-home h2, .title-gallery-home div, #ros-in p').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated bounceInLeft',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.thumbnail-gallery, .hotel-property h3, #image-popups h3, .hotel-info-home h3, .title-gallery-home, body.home .deals .room-thumb, body.home .deals h3').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn', // Class to add to the elements when they are visible
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
jQuery(document).ready(function() {
	jQuery('.box-home-grid, .hotel-info-home p, .headline h1, .box-room').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated zoomIn',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});

jQuery(document).ready(function() {
	jQuery('.thumbn').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated swing',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});

jQuery(document).ready(function() {
	jQuery('li.product').addClass("krs-hidden").viewportChecker({
	    classToAdd: 'krs-visible animated fadeInLeft',
	    offset: 100,
	    repeat: false,
	    callbackFunction: function(elem, action){},
	    scrollHorizontal: false
	});
});
