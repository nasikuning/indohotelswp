<?php
/*-----------------------------------------------*/
/* KARISMA FRAMEWORK
/* Version :1.0.1
/* You can add your own function here! 
/* But don't change anything in this file!
/*-----------------------------------------------*/

/**
* This loader function call in functions.php in root theme.
*/
class KARISMA 
{
	
	public static function init(){
		# code...
		self::krs_define();
		self::krs_functions();
		self::krs_add_action();
		self::krs_add_sorecode();
		self::krs_filters();
		self::krs_remove_action();
	}
	public static function krs_define(){
		define('krs_themes_slug', get_template());
		define('krs_dir', get_template_directory() . '/');
		define('krs_inc', get_template_directory() . '/includes/');
		/*
		* 
		*/
		define('krs_url', get_template_directory_uri() . '/');
		define('krs_style', get_template_directory_uri() . '/includes/');
		if(!defined('karisma_text_domain')){
		define('karisma_text_domain', krs_themes_slug);
		}

	}
	public static function krs_functions(){
		require_once(krs_inc . 'admin/theme-options.php');
		require_once(krs_inc . 'functions/_header_krs.php');
		require_once(krs_inc . 'functions/_core_krs.php');
		require_once(krs_inc . 'functions/_css_krs.php');
		require_once(krs_inc . 'functions/_js_krs.php');
		require_once(krs_inc . 'functions/_share_krs.php');
		require_once(krs_inc . 'functions/_logo_krs.php');
		require_once(krs_inc . 'functions/_featuredimg_krs.php');
		require_once(krs_inc . 'functions/_comment_krs.php');
		require_once(krs_inc . 'functions/_footer_krs.php');
		require_once(krs_inc . 'functions/_customgallery_krs.php');
		require_once(krs_inc . 'functions/_custompost_krs.php');
		require_once(krs_inc . 'functions/_relatedpost_krs.php');
		require_once(krs_inc . 'functions/_meta_box_krs.php');
		require_once(krs_inc . 'functions/_form_booking_krs.php');
		require_once(krs_inc . 'functions/_top_deals_krs.php');
		require_once(krs_inc . 'functions/_styling_krs.php');
		require_once(krs_inc . 'functions/demo_meta.php');
		require_once(krs_inc . 'functions/_cpt_krs.php');
		require_once(krs_inc . 'functions/_acf_krs.php');

		//Widget
		require_once(krs_inc . 'functions/widget/_info_krs.php');

		// Register Custom Navigation Walker
		require_once(krs_inc . 'functions/wp-bootstrap-navwalker.php');
		/**
		* Required: include OptionTree Karisma setting admin and meta boxes.
		*/
		//require_once krs_inc . 'admin/meta-boxes.php';
		

		require_once(krs_inc . 'admin/select/category.php');

	}	
	public static function krs_add_action(){
		// Add Actions
		add_action('init', 'krs_loadjs'); // Add Custom Scripts to wp_head
		add_action('wp_enqueue_scripts', 'krs_styles'); // Add Theme Stylesheet
		add_action('init', 'register_karisma_menu'); // Add karisma Menu
		add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
		add_action('init', 'karisma_pagination'); // Add our karisma Pagination
		add_action('init','krs_load_share');
		add_action( 'wp_head', 'krs_header' );
		add_action( 'wp_head', 'krs_headscript' );
		// add_action( 'wp_head', 'krs_main_color' );
		add_action('init', 'rooms_post'); 
		add_action('init', 'deals_post');
		add_action('init', 'hotel_info');
		add_action('init', 'restaurant_cafe_post'); 
		add_action('init', 'beauty_retail_post'); 
		add_action('init', 'meetings_events_post'); 
		add_action('init', 'load_cpt_krs'); 
		// add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
		add_action( 'pre_get_posts', 'krs_parse_request_trick' ); 
		add_action( 'widgets_init', 'krs_register_sidebars' );
		add_action('upload_mimes', 'add_file_types_to_uploads');
	}
	public static function krs_add_sorecode(){
		// Shortcodes
		add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); 
	}
	public static function krs_filters(){
			// Add Filters
		add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
		add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
		add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
		add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
		add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
		add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
		add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
		add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
		add_filter( 'get_avatar', 'krs_avatar_css' );
		add_filter( 'comment_form_default_fields','krs_com_fields' );
		add_filter( 'comment_form_field_comment','krs_com_fields_textarea' );
		add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
		add_filter('style_loader_tag', 'karisma_style_remove'); // Remove 'text/css' from enqueued stylesheet
		add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
		add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
		add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);
		add_filter('get_image_tag_class', 'image_tag_class' );
		add_filter('excerpt_more', 'wpdocs_excerpt_more' );

		if (!class_exists('OT_Loader')) {
			add_filter('ot_theme_options_parent_slug', '__return_false');
			add_filter('ot_theme_mode', '__return_true');
		/**
		* Required: include OptionTree.
		* http://wordpress.org/extend/plugins/option-tree/
		*/
			require_once krs_inc . 'admin/ot-loader.php';
		}
		/*
		* Optional: set 'ot_show_pages' filter to false.
		* This will hide the settings & documentation pages.
		*/
		// add_filter( 'ot_show_options_ui', '__return_false' );
		// add_filter( 'ot_show_docs', '__return_false' );
		// add_filter( 'ot_show_pages', '__return_false' );	
		// add_filter( 'ot_show_new_layout', '__return_false' );
		// Add it to a column in WP-Admin - (Optional)
		// add_filter('manage_posts_columns', 'posts_column_views');
		add_filter( 'post_type_link', 'krs_remove_cpt_slug', 10, 3 );
		add_filter( 'ot_recognized_font_families', 'filter_ot_recognized_font_families', 10, 2 );
	}
	public static function krs_remove_action(){
		// Remove Actions
			remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
			remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
			remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
			remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
			remove_action('wp_head', 'index_rel_link'); // Index link
			remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
			remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
			remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
			remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
			remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
			remove_action('wp_head', 'rel_canonical');
			remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
		}
	}