<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
function krs_load_share() {
	global $post;
	if(is_singular() && (ot_get_option('krs_active_shared') != 'off')){

		// Get current page URL
		$karismaURL = urlencode(get_permalink());

		// Get current page title
		$karismaTitle = str_replace( ' ', '%20', get_the_title());

		// Get Post Thumbnail for pinterest
		$karismaThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$karismaTitle.'&amp;url='.$karismaURL.'&amp;via=Kentosweb';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$karismaURL;
		$googleURL = 'https://plus.google.com/share?url='.$karismaURL;
		$bufferURL = 'https://bufferapp.com/add?url='.$karismaURL.'&amp;text='.$karismaTitle;
		$whatsappURL = 'whatsapp://send?text='.$karismaTitle . ' ' . $karismaURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$karismaURL.'&amp;title='.$karismaTitle;

		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$karismaURL.'&amp;media='.$karismaThumbnail[0].'&amp;description='.$karismaTitle;

		// Add sharing button at the end of page/page content
		echo '<!-- karisma.com social sharing. Get your copy here: http://karisma.me/1VIxAsz -->';
		echo '<div class="karisma-social">';
		echo '<a class="karisma-link karisma-twitter" href="'. $twitterURL .'" target="_blank">Twitter</a>';
		echo '<a class="karisma-link karisma-facebook" href="'.$facebookURL.'" target="_blank">Facebook</a>';
		echo '<a class="karisma-link karisma-whatsapp" href="'.$whatsappURL.'" target="_blank">WhatsApp</a>';
		echo '<a class="karisma-link karisma-googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
		echo '<a class="karisma-link karisma-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
		echo '<a class="karisma-link karisma-linkedin" href="'.$linkedInURL.'" target="_blank">LinkedIn</a>';
		echo '<a class="karisma-link karisma-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank">Pin It</a>';
		echo '</div>';

	}
}
/*
* Social network icon
* add_action( 'do_krs_header_sn', 'krs_sn' ); in init.php
*/
if ( !function_exists('krs_sn') ) {
function krs_sn($position = 'text-center') {
	if (ot_get_option('krs_head_social_activated') != 'off') :
	if ((ot_get_option('krs_tweet_sn') != '') || (ot_get_option('krs_fb_sn') != '') || (ot_get_option('krs_gplus_sn') != '') || (ot_get_option('krs_in_sn') != '') || (ot_get_option('krs_dribble_sn') != '') || (ot_get_option('krs_flickr_sn') != '') || (ot_get_option('krs_deviant_sn') != '') || (ot_get_option('krs_blogger_sn') != '') || (ot_get_option('krs_vimeo_sn') != '') || (ot_get_option('krs_youtube_sn') != '') || (ot_get_option('krs_rss_sn') != '')) :
		echo '<div class="'. $position .'"><ul class="list-inline">';
			if (ot_get_option('krs_tweet_sn') != '')
				echo '<li class="twitter"><a class="btn-social btn-outline" href="' . ot_get_option('krs_tweet_sn') . '" title="Twitter" rel="nofollow" target="_blank"><span class="icon-twitter"></span></a></li>';
			if (ot_get_option('krs_fb_sn') != '')
				echo '<li class="facebook"><a class="btn-social btn-outline" href="' . ot_get_option('krs_fb_sn') . '" title="Facebook" rel="nofollow" target="_blank"><span class="icon-facebook"></span></a></li>';
			if (ot_get_option('krs_gplus_sn') != '')
				echo '<li class="gplus"><a class="btn-social btn-outline" href="' . ot_get_option('krs_gplus_sn') . '" title="GPlus" rel="nofollow" target="_blank"><span class="icon-gplus"></span></a></li>';
			if (ot_get_option('krs_in_sn') != '')
				echo '<li class="in"><a class="btn-social btn-outline" href="' . ot_get_option('krs_in_sn') . '" title="LinkedIn" rel="nofollow" target="_blank"><span class="icon-linkedin"></span></a></li>';
			if (ot_get_option('krs_dribble_sn') != '')
				echo '<li class="dribble"><a class="btn-social btn-outline" href="' . ot_get_option('krs_dribble_sn') . '" title="Dribble" rel="nofollow" target="_blank"><span class="icon-dribbble"></span></a></li>';
			if (ot_get_option('krs_flickr_sn') != '')
				echo '<li class="flickr"><a class="btn-social btn-outline" href="' . ot_get_option('krs_flickr_sn') . '" title="Flickr" rel="nofollow" target="_blank"><span class="icon-flickr"></span></a></li>';
			if (ot_get_option('krs_instagram_sn') != '')
				echo '<li class="instagram"><a class="btn-social btn-outline" href="' . ot_get_option('krs_instagram_sn') . '" title="Instagram" rel="nofollow" target="_blank"><span class="icon-instagram"></span></a></li>';
			if (ot_get_option('krs_tripadvisor_sn') != '')
				echo '<li class="tripadvisor"><a class="btn-social btn-outline" href="' . ot_get_option('krs_tripadvisor_sn') . '" title="Tripadvisor" rel="nofollow" target="_blank"><span class="icon-tripadvisor"></span></a></li>';
			if (ot_get_option('krs_line_sn') != '')
				echo '<li class="scline"><a class="btn-social btn-outline" href="' . ot_get_option('krs_line_sn') . '" title="Line" rel="nofollow" target="_blank"><span class="icon-line-logo"></span></a></li>';
			if (ot_get_option('krs_tumblr_sn') != '')
				echo '<li class="tumblr"><a class="btn-social btn-outline" href="' . ot_get_option('krs_tumblr_sn') . '" title="Tumblr" rel="nofollow" target="_blank"><span class="icon-tumblr"></span></a></li>';
			if (ot_get_option('krs_youtube_sn') != '')
				echo '<li class="youtube"><a class="btn-social btn-outline" href="' . ot_get_option('krs_youtube_sn') . '" title="YouTube" rel="nofollow" target="_blank"><span class="icon-youtube-play"></span></a></li>';
			if (ot_get_option('krs_rss_sn') == 'on')
				echo '<li class="rss"><a class="btn-social btn-outline" href="' . get_bloginfo('rss2_url') . '" title="RSS" rel="nofollow" target="_blank"><span class="icon-rss"></span></a></li>';
	echo '</ul></div>';
	endif;
	endif;
	}
}
