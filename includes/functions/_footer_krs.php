<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/* 
* Function sub footer for credits or copyright text
*/
if ( !function_exists( 'krs_footer' ) ) :
	function krs_footer() {
		echo '<div class="footercredits">' . ot_get_option('krs_footcredits') . '</div>';
	}
	endif;
	if ( !function_exists( 'krs_mainfooter' ) ) :
		function krs_scriptfooter() {
			$footer_script = ot_get_option('krs_footer');
		}
		endif;