<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

// Load karisma styles
function krs_styles()
{
    $googlefont = isset( ot_get_option('krs_main_fonts')['font-family'] ) ? ot_get_option('krs_main_fonts')['font-family'] : '';

    wp_register_style('karisma-style', krs_url . 'style.css', array(), wp_get_theme()->Version, 'all');
    wp_register_style('bootstrap-min', krs_style . 'vendor/bootstrap/css/bootstrap.min.css', array(), '3.3.7', 'all');
	wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.4/css/all.css', array(), '5.0.4','all' );
    wp_register_style('animate-css', krs_style . 'vendor/animate/css/animate.min.css', array(), '0.8.0', 'all');
    wp_register_style('fontello-css', krs_style . 'vendor/fontello/css/marker.css', array(), '1.0.0', 'all');
    wp_register_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '1.0', 'all');
    wp_register_style($googlefont,'https://fonts.googleapis.com/css?family='. $googlefont, array(), '1.0','all' );
    wp_register_style('owl', krs_style . 'vendor/owl/owl.carousel.min.css', array(), '1.2','all' );
    wp_register_style('owl-theme', krs_style . 'vendor/owl/owl.theme.default.min.css', array(), '1.2','all' );
    wp_register_style('slick', krs_style . 'vendor/slick/slick.min.css', array(), '1.3','all' );
    wp_register_style('slick-theme', krs_style . 'vendor/slick/slick-theme.min.css', array(), '1.3','all' );
    wp_register_style('magnific-popup', krs_style . 'vendor/magnific/magnific-popup.min.css', array(), '1.3','all' );

    wp_enqueue_style('jquery-ui');
    wp_enqueue_style('bootstrap-min');
    wp_enqueue_style('animate-css');
    wp_enqueue_style('fontello-css');
    wp_enqueue_style('font-awesome-min');
    wp_enqueue_style($googlefont);
    wp_enqueue_style('owl' );
    wp_enqueue_style('owl-theme' );
    wp_enqueue_style('slick');
    wp_enqueue_style('slick-theme');
    wp_enqueue_style('karisma-style');
    wp_enqueue_style('magnific-popup');
    }
