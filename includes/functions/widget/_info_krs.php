<?php
add_action('widgets_init', 'krs_info_load_widgets');

function krs_info_load_widgets()
{
	register_widget('Info_Widget');
}

/* ==  Widget ==============================*/

class Info_Widget extends WP_Widget {


/* ==  Widget Setup ==============================*/

	function __construct()
	{
		$widget_ops = array('classname' => 'krs_info_widget', 'description' => __('A widget that displays your Information like address and contact .', karisma_text_domain) );

		$control_ops = array('id_base' => 'krs_info_widget');

		parent::__construct('krs_info_widget', __('IDH: Hotel Information', karisma_text_domain), $widget_ops, $control_ops);
	}


/* ==  Display Widget ==============================*/

	function widget($args, $instance)
	{
		extract($args);

		$title = $instance['title'];
		$categories = $instance['categories'];
		$posts = $instance['posts'];

		echo $before_widget;
		?>

	<?php
		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>


	<?php if(!empty(ot_get_option('krs_address'))) : ?>
		<div class="info-text">
			<div class="info-icon">
				<span class="icon-round">
					<i class="fas fa-map-marker-alt" title="Address"></i>
				</span>
			</div><!-- end .info-icon -->
			<div class="info-content">
				<?php echo ot_get_option('krs_address'); ?>
			</div><!-- end .info-content -->
		</div><!-- end .info-text -->
	<?php endif; ?>

	<?php if(!empty(ot_get_option('krs_phone'))) : ?>
		<div class="info-text">
			<div class="info-icon">
				<span class="icon-round">
					<i class="fas fa-phone" title="Address"></i>
				</span>
			</div><!-- end .info-icon -->
			<div class="info-content">
				<p><?php echo ot_get_option('krs_phone'); ?></p>
			</div><!-- end .info-content -->
		</div><!-- end .info-text -->
	<?php endif; ?>

	<?php if(!empty(ot_get_option('krs_whatsapp'))) : ?>
		<div class="info-text">
			<div class="info-icon">
				<span class="icon-round">
					<i class="fab fa-whatsapp" title="WhatsApp"></i>
				</span>
			</div><!-- end .info-icon -->
			<div class="info-content">
				<?php $wa = 'https://api.whatsapp.com/send?phone=' . ot_get_option('krs_whatsapp') . '&text=Hello,%20hotel%20team.%20I%20need%20your%20help%20for%20my%20booking.'; ?>
				<p><a href="<?php echo $wa; ?>"><?php echo ot_get_option('krs_whatsapp'); ?></a></p>
			</div><!-- end .info-content -->
		</div><!-- end .info-text -->
	<?php endif; ?>

	<?php if(!empty(ot_get_option('krs_bbm'))) : ?>
		<div class="info-text">
			<div class="info-icon">
				<span class="icon-round">
					<i class="fab fa-blackberry" title="BBM"></i>
				</span>
			</div><!-- end .info-icon -->
			<div class="info-content">
				<p><?php echo ot_get_option('krs_bbm'); ?></p>
			</div><!-- end .info-content -->
		</div><!-- end .info-text -->
	<?php endif; ?>

	<?php if(!empty(ot_get_option('krs_email'))) : ?>
		<div class="info-text">
			<div class="info-icon">
				<span class="icon-round">
					<i class="far fa-envelope-open" title="Email"></i>
				</span>
			</div><!-- end .info-icon -->
			<div class="info-content">
				<a href="mailto:<?php echo ot_get_option('krs_email'); ?>">
					<?php echo ot_get_option('krs_email'); ?>
				</a>
			</div><!-- end .info-content -->
		</div><!-- end .info-text -->
	<?php endif; ?>

		<!-- END WIDGET -->
		<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		$instance['posts'] = $new_instance['posts'];

		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Address', 'categories' => 'all', 'posts' => 4);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">
					<?php _e('Title:', karisma_text_domain) ?>
				</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"
				    value="<?php echo $instance['title']; ?>" />
			</p>
			<?php
	}
}
?>
