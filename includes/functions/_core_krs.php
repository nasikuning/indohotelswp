<?php
/*
 *  Author: Amri Karisma | @amrikarisma
 *  URL: kentos.org | @amrikarisma
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
    \*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Theme Support
    \*------------------------------------*/

    if (!isset($content_width))
    {
        $content_width = 900;
    }

    if (function_exists('add_theme_support'))
    {
        // Add Menu Support
        add_theme_support('menus');

        // Add Thumbnail Theme Support
        add_theme_support('post-thumbnails');
        add_image_size('large', 700, '', true);
        add_image_size('medium', 320, 200, true);
        add_image_size('small', 120, '', true);
        add_image_size('full');
        add_image_size( 'admin-list-thumb', 80, 80, true);
        add_image_size( 'album-grid', 450, 450, true );
        add_image_size('gallery-slide', 900, 500, true);
        add_image_size('custom-size', 900, 300, true);
        add_image_size('gallery-slide-main', 1920, 1080, true);

        // Enables post and comment RSS feed links to head
        add_theme_support('automatic-feed-links');

        add_theme_support('title-tag');
            // Localisation Support
        load_theme_textdomain(karisma_text_domain, krs_style . 'vendor/languages');
    }

/*------------------------------------*\
    Functions
    \*------------------------------------*/

// karisma navigation
if ( ! function_exists ( 'karisma_nav_top' ) ) {
    function karisma_nav_top()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'top-menu',
                'menu'            => 'top-menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-left',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            )
        );
    }
}
if ( ! function_exists ( 'karisma_nav_top_mobile' ) ) {
    function karisma_nav_top_mobile()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'top-menu',
                'menu'            => 'top-menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'monav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            )
        );
    }
}
if ( ! function_exists ( 'karisma_nav' ) ) {
    function karisma_nav()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'header-menu',
                'menu'            => 'header-menu',
                'depth'             => 5,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            )
        );
    }
}
if ( ! function_exists ( 'karisma_nav_mobile' ) ) {
    function karisma_nav_mobile()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'header-menu',
                'menu'            => 'header-menu',
                'depth'             => 5,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'monav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => ''
                )
            );
    }
}
if ( ! function_exists ( 'karisma_nav_footer' ) ) {
    function karisma_nav_footer()
    {
        wp_nav_menu(
            array(
                'theme_location'  => 'footer-menu',
                'menu'            => 'footer-menu',
                'container'       => 'div',
                'container_class' => 'menu-{menu slug}-container',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul class="footer-links">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
                )
            );
    }
}
if ( ! function_exists ( 'special_nav_class' ) ) {
    function special_nav_class ($classes, $item) {
        if (in_array('current-menu-item', $classes) ){
            $classes[] = 'active ';
        }
        return $classes;
    }
}
if ( ! function_exists ( 'register_karisma_menu' ) ) {
    // Register karisma Navigation
    function register_karisma_menu()
    {
        register_nav_menus(array( // Using array to specify more menus if needed
            'top-menu' => __('Top Menu', karisma_text_domain), // Main Navigation
            'header-menu' => __('Header Menu', karisma_text_domain), // Main Navigation
            'footer-menu' => __('Footer Menu', karisma_text_domain) // Extra Navigation if needed (duplicate as many as you need!)
            ));
    }
}
// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}


// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

/*-----------------------------------------------------------------------------------*/
/*  Enable Widgetized sidebar and Footer
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists ( 'krs_register_sidebars' ) ) {
    function krs_register_sidebars(){
        if ( function_exists('register_sidebar') ){
            register_sidebar(array(
                'name' => 'Sidebar',
                'description'   => __( 'Appears in Sidebar.', karisma_text_domain ),
                'id' => 'sidebar',
                'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<span class="sidebar-widget-header"><h3 class="sidebar-widget-header">',
                'after_title' => '</h3></span>',
            ));
            register_sidebar(array(
                'name' => 'Footer Left',
                'description'   => __( 'Appears in Footer.', karisma_text_domain ),
                'id' => 'footer1',
                'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<span class="footer-widget-header"><h3 class="footer-widget-header">',
                'after_title' => '</h3></span>',
            ));
            register_sidebar(array(
                'name' => 'Footer Center',
                'description'   => __( 'Appears in Footer.', karisma_text_domain ),
                'id' => 'footer2',
                'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<span class="footer-widget-header"><h3 class="footer-widget-header">',
                'after_title' => '</h3></span>',
            ));
            register_sidebar(array(
                'name' => 'Footer Right',
                'description'   => __( 'Appears in Footer.', karisma_text_domain ),
                'id' => 'footer3',
                'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<span class="footer-widget-header"><h3 class="footer-widget-header">',
                'after_title' => '</h3></span>',
            ));
            register_sidebar(array(
                'name' => 'Header - Translation',
                'description'   => __( 'Language translation in header', karisma_text_domain ),
                'id' => 'box-language',
                'before_widget' => '<div id="%1$s" class="language-widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<div class="lang-widget-header"><h3>',
                'after_title' => '</h3></div>',
            ));
        }
    }
}
// Remove wp_head() injected Recent Comment styles
if ( ! function_exists ( 'my_remove_recent_comments_style' ) ) {
    function my_remove_recent_comments_style()
    {
        global $wp_widget_factory;
        remove_action('wp_head', array(
            $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
            'recent_comments_style'
            ));
    }
}

// function to display number of posts.
if ( ! function_exists ( 'getPostViews' ) ) {
    function getPostViews($postID){
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return "0 View";
        }
        return $count.' Views';
    }
}
// function to count views.
if ( ! function_exists ( 'setPostViews' ) ) {
    function setPostViews($postID) {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        }else{
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
}
// Add it to a column in WP-Admin - (Optional)
if ( ! function_exists ( 'posts_column_views' ) ) {
    function posts_column_views($defaults){
        $defaults['post_views'] = __('Views');
        return $defaults;
    }
}
if ( ! function_exists ( 'posts_custom_column_views' ) ) {
    function posts_custom_column_views($column_name, $id){
        if($column_name === 'post_views'){
            echo getPostViews(get_the_ID());
        }
    }
}
// Add class before iframe
if ( ! function_exists ( 'wrap_embed_with_div' ) ) {
    function wrap_embed_with_div($html, $url, $attr) {
        if( stripos( $html, 'youtube.com' ) !== FALSE && stripos( $html, 'iframe' ) !== FALSE )
            $html = str_replace( '<iframe', '<iframe class="embed-responsive-item"  ', $html );
            $html = '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
        return $html;
    }
}

if ( ! function_exists ( 'image_tag_class' ) ) {
    function image_tag_class($class) {
        $class .= ' img-responsive';
        return $class;
    }
}
if ( ! function_exists ( 'wpdocs_excerpt_more' ) ) {
    function wpdocs_excerpt_more( $more ) {
        return ' <a href="'.get_the_permalink().'" rel="nofollow">Read More...</a>';
    }
}


// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
if ( ! function_exists ( 'karisma_pagination' ) ) {
    function karisma_pagination() {

        if( is_singular() )
            return;

        global $wp_query;

        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
            return;

        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        /** Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;

        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }

        if ( ( $paged + 2 ) <= $max ) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }

        echo '<div class="Page navigation"><ul class="pagination">' . "\n";

        /** Previous Post Link */
        if ( get_previous_posts_link() )
            printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';

            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

            if ( ! in_array( 2, $links ) )
                echo '<li></li>';
        }

        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }

        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) )
                echo '<li></li>' . "\n";

            $class = $paged == $max ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }

        /** Next Post Link */
        if ( get_next_posts_link() )
            printf( '<li>%s</li>' . "\n", get_next_posts_link() );

        echo '</ul></div>' . "\n";

    }
}
//Repalace avatar to gravatar-img
function krs_avatar_css($class) {
    $class = str_replace("class='avatar", "class='gravatar-img", $class) ;
    return $class;
}

//Get avatar URL

function krs_get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function karisma_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}
function get_resolution_images($reso=''){
    $optional_size = '';
    global $post;
    if(!empty($post->ID)) {
        $img_id = get_post_thumbnail_id($post->ID);
        $image = wp_get_attachment_image_src($img_id, $optional_size);
        $reso =  $image[1]. 'x' . $image[2];
    }
    return $reso;
}
add_action('wp', 'get_resolution_images');
// Custom Gravatar in Settings > Discussion
function karismagravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}
function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(https://media.indohotels.id/customer/public/assets/logo-company.svg);
        height:65px;
        width:320px;
        background-size: 320px 65px;
        background-repeat: no-repeat;
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/*
* Get Info Themes
*/
function krs_let_to_num( $size ) {
    $let = substr( $size, -1 );
    $ret = substr( $size, 0, -1 );
    switch( strtoupper( $let ) ) {
        case 'P':
        $ret *= 1024;
        case 'T':
        $ret *= 1024;
        case 'G':
        $ret *= 1024;
        case 'M':
        $ret *= 1024;
        case 'K':
        $ret *= 1024;
    }
    return $ret;
}
function krs_themes_info() {
    if( is_admin() ) {
        $karisma_theme = wp_get_theme();
        $krsinfotheme = '<div class="format-setting-wrap"><div class="format-setting-label"><h3 class="label"><strong>System Information</strong></h3></div>';
        $krsinfotheme .= '<ul>';
        $krsinfotheme .= '<li><strong>Theme Name:</strong> ' . $karisma_theme->Name . '</li>';
        $krsinfotheme .= '<li><strong>Theme Version:</strong> ' . $karisma_theme->Version . '</li>';
        $krsinfotheme .= '<li><strong>Author:</strong> ' . $karisma_theme->get( 'ThemeURI' ) . '</li>';
        $krsinfotheme .= '<li><strong>Home URL:</strong>' . home_url() . '</li>';
        $krsinfotheme .= '<li><strong>Site URL:</strong>' . site_url() . '</li>';
        if ( is_multisite() ) {
            $krsinfotheme .= '<li><strong>WordPress Version:</strong>' . 'WPMU ' . get_bloginfo('version') . '</li>';
        } else {
            $krsinfotheme .= '<li><strong>WordPress Version:</strong>'. 'WP ' . get_bloginfo('version') . '</li>';
        }
        if ( function_exists( 'phpversion' ) ) {
            $krsinfotheme .= '<li><strong>PHP Version:</strong>' . esc_html( phpversion() ) . '</li>';
        }
        if (function_exists( 'size_format' )) {
            $krsinfotheme .= '<li><strong>Memory Limit:</strong>';
            $mem_limit = krs_let_to_num( WP_MEMORY_LIMIT );
            if ( $mem_limit < 67108864 ) {
                $krsinfotheme .= '<mark class="error">' . size_format( $mem_limit ) .' - Recommended memory to at least 64MB. Please see: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a></mark>';
            } else {
                $krsinfotheme .= '<mark class="yes">' . size_format( $mem_limit ) . '</mark>';
            }
            $krsinfotheme .= '</li>';
            $krsinfotheme .= '<li><strong>WP Max Upload Size:</strong>'. size_format( wp_max_upload_size() ) .' - Recommended is 2MB (Find tutorial about it in <a href="https://www.google.com/#q=WP+Max+Upload+Size" target="_blank">Google</a>)</li>';
        }
        if ( function_exists( 'ini_get' ) ) {
            $krsinfotheme .= '<li><strong>PHP Time Limit:</strong>'. ini_get('max_execution_time') .'</li>';
        }
        if ( defined('WP_DEBUG') && WP_DEBUG ) {
            $krsinfotheme .= '<li><strong>WP Debug Mode:</strong><mark class="yes"><b>Yes</b> - If life website please turn off WP debug mode. Please see: <a href="http://codex.wordpress.org/Debugging_in_WordPress" target="_blank">Debugging in WordPress</a></mark></mark></li>';
        } else {
            $krsinfotheme .= '<li><strong>WP Debug Mode:</strong><mark class="no">No</mark></li>';
        }
        $krsinfotheme .= '</ul></div>';

        return $krsinfotheme;
    }
}
function krs_license() {
    if( is_admin() ) {
        $karisma_theme = wp_get_theme();
        $krsinfotheme = '<div class="format-setting-wrap"><div class="format-setting-label"><h3 class="label"><strong>Theme License</strong></h3></div>';
        $krsinfotheme .= '<ol>';
        $krsinfotheme .= '<li>The PHP code is licensed under the GPL license as is WordPress itself. <a href="http://codex.wordpress.org/GPL/">http://codex.wordpress.org/GPL/</a></li>';
        $krsinfotheme .= '<li>This is single unlimited domain for own domain only <strong>cannot resale theme or install for other people</strong>. Require: place the single license in theme folder.</li>';
        $krsinfotheme .= '<li>Intellectual license from <a href="https://indohotels.id">indohotels.id</a></li>';
        $krsinfotheme .= '</ol></div>';

        return $krsinfotheme;
    }
}


?>
