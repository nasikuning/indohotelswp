<?php

// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

if ( ! class_exists( 'KrsCustomPosts' ) ) {

    class KrsCustomPosts {
    
        function __construct($init) {
    
            $this->settings = $init;
    
            add_action( 'init', array(&$this, 'add_custom_post_type') );
    
        }
    
        function add_custom_post_type() {
    
            register_post_type( $this->settings['slug'],
    
            array(
                'labels' => array(
                    'name' => __( $this->settings['name'] ),
                    'singular_name' => __( $this->settings['singular_name'] )
                ),
    
            'public' => $this->settings['is_public'],
    
            'has_archive' => $this->settings['has_archive'],
            )
    
        );
    
        }

    }



}
if ( !function_exists('load_cpt_krs') ) :
    function load_cpt_krs(){
        $titleCpt = ot_get_option('krs_cpt_name');
        $slugCpt = str_replace(' ', '_',  $titleCpt);
        // echo '<pre>';
        // print_r($cptOptions['name']);
        // echo '</pre>';

        $custom_posts = array(
            "slug" => $slugCpt,
            "name" =>  $titleCpt,
            "singular_name" => $titleCpt,
            "is_public" => true,
            "has_archive" => false,
        );
        $var = new KrsCustomPosts ($custom_posts);     
    }
endif;