<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

if ( !function_exists('header_cover') ) :
	function krs_header_cover() {
		if ( has_post_thumbnail()) : // Check if thumbnail exists
		$coverimage = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "cover" );
		$background_header = 'style="background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(' .$coverimage[0] .');"';

		echo $background_header;
		endif;
	}
endif;

function krs_googlefonts(){

    $key = 'krs_google_fonts';

    // Let's see if we have a cached version
    $google_fonts = get_transient($key);
    if ($google_fonts) {
        return $google_fonts;
    } else {
        // If there's no cached version we ask Google for new fonts
        $fontsSeraliazed = "https://www.googleapis.com/webfonts/v1/webfonts?key=".ot_get_option('google-font-api') ."&sort=popularity";
        $response = wp_remote_get ( $fontsSeraliazed, array ( 'sslverify' => false ) );
        if (is_wp_error($response)) {
            // In case connection fails we get last stored key
            return get_option($key);
        } else {
            // If everything's okay, parse the body and json_decode it
            $fontArray = json_decode($response['body'], true);
            // Get only 100 first fonts this can be modified or removed
            $sliced_fonts = array_slice($fontArray['items'], 0, 100);

            if ( $sliced_fonts ) {

                $googleFontArray = array();
                // generate the array to store
                foreach($sliced_fonts as $index => $value){
                    $_family = strtolower( str_replace(' ','_',$value['family']) );
                    $googleFontArray[ str_replace(' ', '+', $value['family']).':'.implode(',', $value['variants']) ] = $value['family'];

                }

            }

            // Store the result in a transient, expires after 1 day
            // Also store it as the last successful using update_option
            set_transient($key, $googleFontArray, 60*60*24);
            update_option($key, $googleFontArray);
            return $googleFontArray;
        }
    }
}
function filter_ot_recognized_font_families( $array, $field_id ) {


    $theme_fonts = array(
        'arial'         => 'Arial, Helvetica, sans-serif',
        'helvetica'     => '"Helvetica Neue", Helvetica, Arial, sans-serif',
        'georgia'       => 'Georgia, "Times New Roman", Times, serif',
        'tahoma'        => 'Tahoma, Geneva, sans-serif',
        'times'         => '"Times New Roman", Times, serif',
        'trebuchet'     => '"Trebuchet MS", Arial, Helvetica, sans-serif',
        'verdana'       => 'Verdana, Geneva, sans-serif'
    );
    if(is_array(krs_googlefonts())){
        $array = array_merge($theme_fonts, krs_googlefonts() );
    } else {
        $array = array_merge($theme_fonts, (array)krs_googlefonts() );
    }
    return $array;

}
