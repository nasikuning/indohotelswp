<?php /* 
$mainColor
$seColor = Secondary Color
$terColor = tertiary Color

== Change Body typography == 

$font_body =  $fontsColor['font-family']
$fontsColor['font-color'] = Body typography Color
$fontHover = Hover color text

*/?>
<style id="indohotels-style" type="text/css">
	body {
		font-family: <?php echo $font_body; ?>;
		color: <?php echo $fontsColor['font-color']; ?>;
	}
	.nav-background, .nav-no-image {
		background-color: <?php echo $mainColor; ?>
	}

	
	.footer-distributed .footer-links li:hover a, a:hover {
		color: <?php echo $fontHover; ?>;

	}
	.nav ul.dropdown-menu,.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline,
	.btn-check, .btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}
	.btn-outline:hover, .btn-outline:focus, .btn-outline:active, .btn-outline.active,
	.book-room:hover,.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left, .owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $seColor; ?>;
	}
	::selection {
		background: <?php echo $mainColor; ?>;
	}
	button, html input[type=button], input[type=reset], input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}
	.booking .form-control, .booking .form-control[readonly], .booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;

	}
	.box-gallery {
		border-top: 2px solid <?php echo $mainColor; ?>;
		border-bottom: 2px solid <?php echo $mainColor; ?>;
	}

	/* Secoundary Color */
	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}
	/* Background Image */



	#ros-in .box-room {
		background-color: <?php echo $terColor; ?>;
	}
	/* Font Color */
	.booking .booking-title,
	#ros-in h2 {
		color:#fff;
	}

	.home-carousel {
		border-bottom: 1px solid <?php echo $mainColor; ?>;
	}

	.box-home-grid .overlay {
		background-color: <?php echo $terColor; ?>;
	}
	/* Facilities */
	.box-text h4 {
		color: <?php echo $seColor; ?>;
	}
	.box h4 {
		background-color: <?php echo $mainColor; ?>;
	}
	.box-home-grid {
		background-color: <?php echo $terColor; ?>;
	}

	/* ROOMS */
	.room-thumb:hover,.room-thumb:active {
		background-color: <?php echo $mainColor; ?>;
		color: #fff;
	}
	.room-details h3.room-details-f-title {
		color: <?php echo $seColor; ?>;
	}
	/* Footer */
	footer .nav-bottom, nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}
	.footer-distributed {
		background-color: <?php echo $mainColor; ?>;
	}
	.footer-distributed .krs_info_widget  p a {
		color: <?php echo $seColor; ?>;
	}
	.footer-distributed .krs_info_widget i {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a:hover {
		background-color: <?php echo $mainColor; ?>;
		color: #ffffff;
		border: 2px solid <?php echo $mainColor; ?>;
	}

	.line {
		background: <?php echo $mainColor; ?>;
	}


	/* Caleran */
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start, .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end {
		background-color: <?php echo $seColor; ?> !important;		
	}
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day {
		color: <?php echo $seColor; ?>;	
	}
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-separator {
		color: <?php echo $seColor; ?>;	
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-today, .caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-today {
		color: <?php echo $seColor; ?>;	
	}
	.caleran-container-mobile .caleran-input .caleran-footer button.caleran-cancel {
		border: 1px solid <?php echo $seColor; ?> !important;
		color: <?php echo $seColor; ?> !important;
	}
	.caleran-apply-d, .caleran-apply {
		background: <?php echo $seColor; ?> !important;
    	border: 1px solid <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick, .caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick {
		background: <?php echo $seColor; ?>;

	}
</style>