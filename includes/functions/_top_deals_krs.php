<?php function top_deals($title='Deals'){ ?>
<?php
	$args = array('post_type' => 'deals');
	query_posts($args);
	if (have_posts()): ?>
	<section class="deals box-deals">
		<div class="container">
			<div class="heading-box">
				<h2><?php _e($title, karisma_text_domain); ?></h2>
			</div>
			<div class="row">
				<?php while (have_posts()) : the_post(); ?>
				<div class="col-md-4">
					<div class="room-thumb">
						<div id="post-<?php the_ID(); ?>" <?php post_class( 'deals-post'); ?>>
							<!-- post title -->

							<!-- post thumbnail -->
							<div class="thumb-deals">
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<a href="<?php the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>">
									<?php the_post_thumbnail('large'); // Declare pixel size you need inside the array ?>
								</a>
								<?php endif; ?>
							</div>
							<!-- /post thumbnail -->
						</div>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php }

function footer_slide(){ ?>
	<?php
	$args = array(
		'post_type' => 'hotel-info',
		'category_name' => 'testimonial'
		);
	query_posts($args);
	if (have_posts()) : ?>
		<section class="hotel-info-home">
			<div class="container">
				<div class="home-award-title text-center">
					<div class="heading-box">
						<h2><?php _e('Testimonials', karisma_text_domain); ?></h2>
					</div>
				</div>
				<div class="home-carousel owl-carousel home-text-slide">
					<?php while (have_posts()) : the_post(); ?>
					<div class="item thumb">
						<div class="slide-awards">
							<!-- post thumbnail -->
							<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
							<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
							<?php endif; ?>
							<!-- /post thumbnail -->
						</div>
						<!-- post title -->
						<div class="title-slide">
							<div>
								<?php the_content(); ?>
							</div>
							<h2>
								<?php echo get_the_title(); ?>
							</h2>
						</div>
					</div>
					<!-- /post title -->
					<?php endwhile; ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		<?php }
