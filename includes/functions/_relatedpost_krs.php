<?php
// Do not load directly...
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

function krs_related_post() {
	if ( ot_get_option('krs_active_related') == 'on' ) :
	global $post;
	global $page_num;
	 $related_count = ot_get_option('krs_taxonomy_count');
	$orig_post = $post;
	if (ot_get_option('krs_taxonomy_relpost') == 'tags')
	{
		$tags = wp_get_post_tags($post->ID);
		if ($tags) {
			$tag_ids = array();
			foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
			$args=array(
				'post_type' => array('post', 'rooms'),
				'tag__in' => $tag_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> $related_count,
				'ignore_sticky_posts'=>1
				);
		}
	}
	elseif (ot_get_option('krs_taxonomy_relpost') == 'category')
	{
		$categories = get_the_category($post->ID);
		if ($categories) {
			$category_ids = array();
			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			$args=array(
				'post_type' => array('post', 'rooms'),
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> $related_count,
				'ignore_sticky_posts'=>1
				);
		} 
	}
	else
	{
		$mbuhopo = substr($post->post_title,0,100);
		if(substr($mbuhopo,-1)==' ') 
		{
			$mbuhopo = substr($mbuhopo,0,-1);
		}	
		if ($pagenum='') $pagenum =1;
		$mbuhopo = explode(' ',$mbuhopo);
		$mbuhopo = $mbuhopo[0];
		$args = '&orderby=rand&showposts='.$related_count.'&s='.$mbuhopo.'&paged='.$page_num;
		query_posts($args);

	}
	if (!isset($args)) $args = '';
	$theme_query = new WP_Query($args);
	
	if ($theme_query->have_posts()) : 		
		while ($theme_query->have_posts() && ! is_page()) : $theme_query->the_post();	?>

            <div class="list-room">
              <div class="outer-img">
					<?php if ( has_post_thumbnail()) { // Check if thumbnail exists ?>
						<?php the_post_thumbnail(array(320,200)); // Declare pixel size you need inside the array ?>
					<?php }  else { ?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/no-image.png" class="attachment-320x200 size-320x200 wp-post-image"
						alt="no-image">
					<?php	} ?>
              </div>
              <div class="room-name">
                <div class="outer-room-name">
                  <h4><?php the_title(); ?></h4>
					<a class="btn-main" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">ROOM DETAILS</a>
                </div>
              </div>
            </div>



	<?php endwhile; ?>
	<?php else : 
	echo '<div class="no-post">No related post!</div>';
	endif; 
	endif;
}
wp_reset_query(); ?>