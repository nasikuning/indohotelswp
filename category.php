<?php get_header(); ?>

	<main role="main">
		<!-- section -->
	<section class="container">

			<h1><?php _e( 'Categories for ', karisma_text_domain ); single_cat_title(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
